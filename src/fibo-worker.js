const {  parentPort, workerData } = require('worker_threads');

function _fibonacci(n) {
    if (n < 2)
        return 1;
    else
        return _fibonacci(n - 2) + _fibonacci(n - 1);
}

let startTs = Date.now();
fiboResult = _fibonacci(workerData.fiboNum);
let completeTs = Date.now();

let result = {
    fibo: fiboResult,
    acceptTs: startTs,
    completeTs: completeTs
};

parentPort.postMessage(result);

// parentPort.on('message', (msg) => {
//     let startTs = Date.now();
//     fiboResult = _fibonacci(msg.fiboNum);
//     let completeTs = Date.now();

//     let result = {
//         fibo: fiboResult,
//         acceptTs: startTs,
//         completeTs: completeTs
//     };

//     parentPort.postMessage(result);
// });
