const { Worker, parentPort, workerData } = require('worker_threads');
const express = require('express');
const http    = require('http');
const path    = require("path");
const timesyncServer = require('timesync/server');
const server  = express();
const PORT    = 3000;

let queueNumBlocking = 1;
let queueNumNonBlocking = 1;

// serve html and other static files
server.use(express.static(__dirname + '/public'));

// enable CORS & cache-control
server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Cache-Control', 'no-cache');
    next();
});

// use time sycn
server.use('/timesync', timesyncServer.requestHandler);

// serve main page
server.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

server.get('/get-fibo-blocking/:num', function(req, res) {
    let fiboNum = parseInt(req.params.num);
    let startTs = Date.now();
    fiboResult = _fibonacci(fiboNum);
    let completeTs = Date.now();

    let result = {
        queueNum: queueNumBlocking,
        fibo: fiboResult,
        acceptTs: startTs,
        completeTs: completeTs
    };
    res.json(result);
    queueNumBlocking++;
});

server.get('/get-fibo-non-blocking/:num', function(req, res) {
    let fiboNum = parseInt(req.params.num);
    function startWorker(path, cb) {
        let w = new Worker(path, { workerData: { fiboNum } });
        w.on('message', (msg) => {
            cb(null, msg);
        });
        w.on('error', cb);
        w.on('exit', (code) => {
            if (code != 0) {
                console.error(new Error(`Worker stopped with exit code ${code}`));
            }
        });
        return w;
    }

    let myWorker = startWorker(__dirname + '/fibo-worker.js', (err, result) => {
        if(err) {
            console.error(err);
            res.status(500).send(err);
        }
        result.queueNum = queueNumNonBlocking;
        res.json(result);
        queueNumNonBlocking++;
    });
});

function _fibonacci(n) {
    if (n < 2)
        return 1;
    else
        return _fibonacci(n - 2) + _fibonacci(n - 1);
}

http.createServer(server).listen(PORT, () => {
    console.log(`Listening on ${PORT}`);
});
